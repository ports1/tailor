Tailor
=
Tailor is a tool to migrate changesets between Aegis, ArX, Bazaar, Bazaar-NG,
CVS, Codeville, Darcs, Git, Mercurial, Monotone, Perforce, Subversion,
and Tla repositories.

Development is hosted at [tailor on GitLab](https://gitlab.com/ports1/tailor/)
